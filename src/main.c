#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "hmi_msg.h"
#include "uart-wrapper.h"
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#define BAUD 9600


#define BLINK_DELAY_MS 100

volatile uint16_t time;

static inline void init_sys_clock(void)
{
    TCCR5A = 0;
    TCCR5B = 0;
    TCCR5B |= _BV(WGM52) | _BV(CS52);
    OCR5A = 62549;
    TIMSK5 |= _BV(OCIE5A);
}

static inline void one_init()
{
    DDRA |= _BV(DDA3);
    uart0_init(UART_BAUD_SELECT(BAUD, F_CPU));
    uart3_init(UART_BAUD_SELECT(BAUD, F_CPU));
    init_sys_clock();
    sei();
    stdout = stdin = &uart0_io;
    stderr = &uart3_out;
    lcd_init();
    lcd_clrscr();
}

static inline void print_ver()
{
    fprintf_P(stderr, PSTR(VER_FW),
              PSTR(GIT_DESCR), PSTR( __DATE__), PSTR( __TIME__));
    fprintf_P(stderr, PSTR(VER_LIBC), PSTR(__AVR_LIBC_VERSION_STRING__));
}

static inline void print_star_inf()
{
    fprintf_P(stdout, PSTR(STUD_NAME "\n"));
    lcd_puts_P(PSTR(STUD_NAME));
    print_ascii_tbl(stdout);
    unsigned char ascii[128] = {0};

    for (unsigned char i = 0; i < sizeof(ascii); i++) {
        ascii[i] = i;
    }

    print_for_human(stdout, ascii, sizeof(ascii));
    fprintf_P(stdout, PSTR(TAKE_LETTER_MONTH));
}

static inline void search_mon()
{
    char month_first_leter;
    fscanf(stdin, "%c", &month_first_leter);
    fprintf(stdout, "%c\n", month_first_leter);
    lcd_goto(0x40);

    for (int i = 0; i < 6; i++) {
        if (!strncmp_P(&month_first_leter, (PGM_P)pgm_read_word(&month_table[i]), 1)) {
            fprintf_P(stdout, PSTR("%S\n"), (PGM_P)pgm_read_word(&month_table[i]));
            lcd_puts_P((PGM_P)pgm_read_word(&month_table[i]));
            lcd_putc(' ');
        }
    }

    fprintf_P(stdout, PSTR(TAKE_LETTER_MONTH));

    for (int i = 0; i < 16; i++) {
        lcd_putc(' ');
    }
}

static inline void heartbeat()
{
    static uint16_t last_time;
    uint16_t current_time = time;

    if ((last_time - current_time) > 0) {
        PORTA ^= _BV(PORTA3);
        fprintf_P(stderr, PSTR(UPTIME "\n"), current_time);
    }

    last_time = current_time;
}

int main (void)
{
    one_init();
    print_ver();
    print_star_inf();

    while (1) {
        heartbeat();

        if (uart0_available()) {
            search_mon();
        }
    }
}

ISR(TIMER5_COMPA_vect)
{
    time++;
}
